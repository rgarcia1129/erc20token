var MyToken = artifacts.require("./MyToken.sol");

module.exports = function(deployer) {
  const _name = 'PaySo';
  const _symbol = 'PSO';
  const _decimals = 18;


  deployer.deploy(MyToken, _name, _symbol, _decimals);
};
