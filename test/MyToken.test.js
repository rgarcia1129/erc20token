const BigNumber = web3.BigNumber;
const MyToken = artifacts.require('MyToken');
require ('chai').use(require("chai-bignumber")(BigNumber)).should();

contract('MyToken', accounts => {
  const _name = 'PaySo';
  const _symbol = 'PSO';
  const _decimals = 18;

  describe('token attributes', () => {

    beforeEach( async () => {
      this.payso = await MyToken.new(_name, _symbol, _decimals)
    })

    it('has the correct name', async () => {
      const name = await this.payso.name();
      name.should.equal(_name);
    })

    it('has the correct symbol', async () => {
      const symbol = await this.payso.symbol();
      symbol.should.equal(_symbol);
    })

    it('has the correct decimals', async () => {
      const decimals = await this.payso.decimals();
      const words = decimals.words[0]
      words.should.be.bignumber.equal(_decimals)
    })

  })
})
